from sre_parse import fix_flags
import numpy as np
import pandas as pd
import geopandas as gpd
import ast
import plotly.express as px
import pyproj
from dash import Dash, html, dcc, Input, Output
import os

import warnings
#ignore by message
warnings.filterwarnings("ignore", message="This function is deprecated. See: https://pyproj4.github.io/pyproj/stable/gotchas.html#upgrading-to-pyproj-2-from-pyproj-1")

def packDatasets(startYear, endYear, datasetsFolder, datasetsPrefix):

    listYears = list(range(startYear, endYear + 1))
    listDatasets = []
    for year in listYears:
        fp = datasetsFolder + "\\" + datasetsPrefix + str(year) + ".txt"
        toPack = pd.read_csv(fp, sep = ";")
        toPack.columns = [el.replace(" ", "") for el in list(toPack.columns)]
        listDatasets.append(toPack)

    return dict(zip(listYears, listDatasets))

def getCommuterEvolutions(MCDF):
    y = []
    x = range(2012,2023)
    for year in x:
        y.append(len(set(MCDF[year].commuterName)) - len(set(MCDF[year - 1].commuterName)))
    
    return x,y

def create_nCommutersGraph(MCDF):
    x,y = getCommuterEvolutions(MCDF)

    
    fig_nCommuters = px.scatter(x=x,y=y)
    fig_nCommuters.update_traces(mode='lines+markers')

    fig_nCommuters.update_xaxes(title="Year") 
    fig_nCommuters.update_yaxes(title="Commuter amount evolution") 

    return fig_nCommuters

def getTramAttributes(tramDF, chosenN):
    
    uniqueTramNames = set(tramDF.tramName)
    df = tramDF[tramDF.cycleNo <= chosenN]    
    outdf = pd.DataFrame()
    for tn in uniqueTramNames :
        newrow = df[df.tramName == tn].tail(1)        
        outdf = pd.concat([outdf, newrow])
    return outdf

def createTramMap(locDF, fieldName = "location", zoomLevel = 14, heightLevel = 400):
    locs = locDF[fieldName]
    outDF = locDF
    outDF["latN"] = [ast.literal_eval(loc)[0] for loc in locs]
    outDF["lonN"] = [ast.literal_eval(loc)[1] for loc in locs]

    # Return generated map.    
    tramMap = px.scatter_mapbox(outDF, lat="latN", lon="lonN", hover_name="tramName",
                        hover_data=["nPassengers"],
                        color_discrete_sequence=["blue"], zoom = zoomLevel, height = heightLevel)
    tramMap.update_layout(mapbox_style="open-street-map")
    tramMap.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

    return tramMap

def createTramGraph(tramDF, tn):
    x,y = getSpecificTramGraphData(tramDF, tn)

    figTrams = px.scatter(x=x,y=y)
    figTrams.update_traces(mode='lines+markers')

    figTrams.update_xaxes(title="Cycle") 
    figTrams.update_yaxes(title="Number of passengers inside " + tn) 

    return figTrams

def getSpecificTramGraphData (tramDF, tn):
    specificDF = tramDF[tramDF.tramName==tn].sort_values(by="cycleNo")
    x = list(specificDF.cycleNo)
    y = list(specificDF.nPassengers)
    return x, y

def commuterStats (MDF, year):
    df = MDF[year]
    # Proportion of journeys considering tram
    out0 = len(df[df.considersTram]) / len(df)
    # Among journeys considering tram
        # Journeys by foot
    out1 = len(df[(df.considersTram) & (df.transportationMethod=="foot")]) / len(df[df.considersTram])
        # Journeys by tram
    out2 = len(df[(df.considersTram) & (df.transportationMethod=="tram")]) / len(df[df.considersTram])
        # Journeys by car
    out3 = len(df[(df.considersTram) & (df.transportationMethod=="car")]) / len(df[df.considersTram])
        # Journeys by tram / journeys by car assuming considering tram is true.
    out4 = len(df[(df.considersTram) & (df.transportationMethod=="tram")]) / len(df[(df.considersTram) & (df.transportationMethod=="car")])
    return [out0, out1, out2, out3, out4]

def commuterStatsAllTime (MDF, startYear, endYear):
    listYears = list(range(startYear, endYear + 1))
    allTimeStats = {"consideringTram":[], "foot":[], "tram":[],"car":[], "tram/car":[]}
    for year in listYears :
        yearlyStats = commuterStats(MDF, year)
        for i, stat in enumerate(yearlyStats):
            allTimeStats[list(allTimeStats.keys())[i]].append(stat)
    return allTimeStats

def clockToCycle(h, m, y, yearTransitions, secondsPerCycle = 10, startingHour = 6):
    return int(1/secondsPerCycle * (3600*(h-startingHour) + 60*m)) + int(yearTransitions[yearTransitions.year == y].cycleNo)

def cycleToClock(cNo, yearTransitions, secondsPerCycle = 10, startingHour = 6):
    correspondingYearEntry = yearTransitions[yearTransitions.cycleNo <= cNo].tail(1)
    y = int(correspondingYearEntry.year)
    timeInfo = cNo - int(correspondingYearEntry.cycleNo)
    timeInfo *= secondsPerCycle
    h = timeInfo // 3600
    m = (timeInfo - h*3600) // 60
    h += startingHour
    return h,m,y

        
workspace = os.path.dirname(os.getcwd())
commuterWS = workspace + "\\commuterData"
roadWS = workspace + "\\roadData"
tramWS = workspace + "\\tramData"
yearTransitionsFile = workspace + "\\yearTransitions.txt"

mainCDF = packDatasets(2010,2022,commuterWS, "commuterData")
print("Commuter data packed.")
mainTDF = packDatasets(2010,2022,tramWS, "tramData")
print("Tram data packed.")
mainRDF = packDatasets(2010,2022,roadWS, "roadData")
print("Road data packed.")
yearTransitions = pd.read_csv(yearTransitionsFile, sep=";")

listIndicators = ["Proportion of journeys considering the tram.",
        "Proportion of journeys by foot, assuming tram considered.",
        "Proportion of journeys by tram, assuming tram considered.",
        "Proportion of journeys by car, assuming tram considered.",
        "#Journeys by tram divided by #Journeys by car, assuming tram considered."]



allTimeStats = commuterStatsAllTime(mainCDF, 2010, 2022)
initTramDF = getTramAttributes(mainTDF[2010],1000)
tramMap = createTramMap(initTramDF)
tramGraph = createTramGraph(mainTDF[2010], "Trams181")
nCommutersGraph = create_nCommutersGraph(mainCDF)

######################
# Dash vizualisation #
######################

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([

    # Map with tram locations.
    html.Div([
        dcc.Graph(id = "tram_map",
        figure=tramMap)
    ]),

    # Cycle selection.
    html.Div([
        html.Label("Converted cycle:"),
            dcc.Input(
            id='cycle_select',
            type='number',
            value=0
        ),

        html.Label("Enter year number..."),
            dcc.Input(
            id='year_select',
            type='number',
            value=2010
        ),
        html.Label("Enter hour number..."),
            dcc.Input(
            id='hour_select',
            type='number',
            value=10
        ),
        html.Label("Enter minute number..."),
            dcc.Input(
            id='minute_select',
            type='number',
            value=00
        ),
    ]),

    # Graph of amount of passengers inside a tram.
    html.Div([
        dcc.Graph(id = "tram_graph",
        figure = tramGraph
        )
    ]),

    # Commuter graph selection dropdown menu.
    html.Div([
        html.Label("Select indicator to plot..."),
        dcc.Dropdown(listIndicators,
        id = "indicators_dropdown")
        
    ]),

    # Commuter indicator graph.
    html.Div([
        dcc.Graph(id='indicators_graph'),
    ]),

    # Commuter amount evolution graph.
    html.Div([
        dcc.Graph(id='nCommuters_graph',
        figure = nCommutersGraph)
    ]),


])

@app.callback(
    Output('cycle_select', 'value'),
    Input("year_select", "value"),
    Input('hour_select', 'value'),
    Input('minute_select', 'value')
)

def updateCycle (y,h,m):
    return clockToCycle(h,m,y, yearTransitions)

# Interactive graphing : select an indicator, update the graph.
@app.callback(
    Output('indicators_graph', 'figure'),
    Input("indicators_dropdown", "value")
)
# Associated interactive graphing function :
# Get corresponding indicator, get commuter stats as y data, get years as x data.
def updateCommutersIndicatorsGraph (indicator):
    i = listIndicators.index(indicator)
    allTimeStats = commuterStatsAllTime(mainCDF, 2010, 2022)
    y = allTimeStats[list(allTimeStats.keys())[i]]
    x = list(range(2010,2023))

    figCommuters = px.scatter(x=x,y=y)
    figCommuters.update_traces(mode='lines+markers')

    figCommuters.update_xaxes(title="Year") 
    figCommuters.update_yaxes(title="Proportion") 

    return figCommuters

# Interactive map of trams, updates on year selection and cycle selection.
@app.callback(
    Output('tram_map', 'figure'),
    Input('year_select', 'value'),
    Input('cycle_select', 'value')
    )

def updateTramMap (year, cycle, zoomLevel = 14, heightLevel = 400):
    tramDF = getTramAttributes(mainTDF[year],int(cycle))
    return createTramMap(tramDF)

# Interactive graph of number of passengers inside a tram, updates on clicking on a tram, and year selection.
@app.callback(
    Output('tram_graph', 'figure'),
    Input("tram_map", "clickData"),
    Input('year_select', 'value')
)

def updateTramGraph (clickData, year = 2010): 
    return createTramGraph(mainTDF[year], clickData['points'][0]['hovertext'])


if __name__ == '__main__':
    app.run_server(debug=True)


